﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using axadotestekhaled.Domain.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.ComponentModel.DataAnnotations.Schema;
using axadotestekhaled.DataAccess.Map;

namespace axadotestekhaled.DataAccess
{
    public class DataContext : DbContext
    {
       
        public DataContext() : base("DataContext")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
        
        public DbSet<Transportadora> Transportadoras { get; set; }
        public DbSet<Frete> Fretes { get; set; }
        public DbSet<Cliente> Clientes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Properties()
                   .Where(p => p.Name == "Id" + p.ReflectedType.Name)
                   .Configure(p => p.IsKey().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity));
            modelBuilder.Properties<string>()
                   .Configure(p => p.HasColumnType("varchar"));
            modelBuilder.Properties<string>()
                  .Configure(p => p.HasMaxLength(150));


            modelBuilder.Configurations.Add(new ClienteMap());
            modelBuilder.Configurations.Add(new TransportadoraMap());
            modelBuilder.Configurations.Add(new FreteMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
