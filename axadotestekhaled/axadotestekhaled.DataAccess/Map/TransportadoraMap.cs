﻿using axadotestekhaled.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace axadotestekhaled.DataAccess.Map
{
    public class TransportadoraMap : EntityTypeConfiguration<Transportadora>
    {
        public TransportadoraMap()
        {
            ToTable("Transportadora");
            HasKey(x => x.IdTransportadora);
            //Property(x => x.IdTransportadora).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.NomeTransportadora).HasMaxLength(150).IsRequired();
        }
    }
}
