﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace axadotestekhaled.Domain.Entities
{
    public class Cliente
    {
        [Key]
        public int IdCliente { get; set; }
        public string NomeCliente { get; set; }
    }
}
