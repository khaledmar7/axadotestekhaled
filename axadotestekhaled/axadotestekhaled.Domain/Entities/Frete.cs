﻿
using System.ComponentModel.DataAnnotations;

namespace axadotestekhaled.Domain.Entities
{
    public class Frete
    {
        
        [Key]
        public int IdFrete { get; set; }
        public int IdCliente { get; set; }
        public int IdTransportadora { get; set; }
        public decimal Taxa { get; set; }

        public virtual Transportadora Transport { get; set; }
        public virtual Cliente Client { get; set; }
        

    }
}
