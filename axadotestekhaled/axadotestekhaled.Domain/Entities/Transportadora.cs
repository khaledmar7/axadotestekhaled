﻿
using System.ComponentModel.DataAnnotations;

namespace axadotestekhaled.Domain.Entities
{
    public class Transportadora
    {
        [Key]
        public int IdTransportadora { get; set; }
        public string NomeTransportadora { get; set; }

    }
}
