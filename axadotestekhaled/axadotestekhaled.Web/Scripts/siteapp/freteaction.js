﻿
function carregaClientes()
{
    var idtransportadora = $("#IdTransportadora").val();

    $.ajax({
        type: "GET",
        data: { idtransportadora: idtransportadora },
        dataType: "json",
        url: BASE_URL + "Fretes/GetClientes/",
        success: function (jsonObj) {
            $('#IdCliente option[value!=""]').remove();
            
            $.each(jsonObj, function (key, objItem) {

                $("#IdCliente").append('<option value="' + objItem.IdCliente + '">' + objItem.NomeCliente + '</option>');

            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            /*
            console.log(XMLHttpRequest);
            console.log(textStatus);
            console.log(errorThrown);
            */
        }

    });
}

$(document).ready(function () {

    $("#IdTransportadora").change(function (e) {
        e.preventDefault();
        carregaClientes();
    });

    
});