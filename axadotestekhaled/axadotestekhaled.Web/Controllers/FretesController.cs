﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using axadotestekhaled.DataAccess;
using axadotestekhaled.Domain.Entities;

namespace axadotestekhaled.Web.Controllers
{
    public class FretesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Fretes
        public ActionResult Index()
        {
            var fretes = db.Fretes.Include(f => f.Client).Include(f => f.Transport);
            return View(fretes.ToList());
        }

        // GET: Fretes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Frete frete = db.Fretes
                .Find(id);
            if (frete == null)
            {
                return HttpNotFound();
            }
            return View(frete);
        }

        // GET: Fretes/Create
        public ActionResult Create()
        {
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "NomeCliente");
            ViewBag.IdTransportadora = new SelectList(db.Transportadoras, "IdTransportadora", "NomeTransportadora");
            return View();
        }

        // POST: Fretes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdFrete,IdCliente,IdTransportadora,Taxa")] Frete frete)
        {
            if (ModelState.IsValid)
            {
                db.Fretes.Add(frete);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "NomeCliente", frete.IdCliente);
            ViewBag.IdTransportadora = new SelectList(db.Transportadoras, "IdTransportadora", "NomeTransportadora", frete.IdTransportadora);
            return View(frete);
        }

        // GET: Fretes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Frete frete = db.Fretes.Find(id);
            if (frete == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "NomeCliente", frete.IdCliente);
            ViewBag.IdTransportadora = new SelectList(db.Transportadoras, "IdTransportadora", "NomeTransportadora", frete.IdTransportadora);
            return View(frete);
        }

        // POST: Fretes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdFrete,IdCliente,IdTransportadora,Taxa")] Frete frete)
        {
            if (ModelState.IsValid)
            {
                db.Entry(frete).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCliente = new SelectList(db.Clientes, "IdCliente", "NomeCliente", frete.IdCliente);
            ViewBag.IdTransportadora = new SelectList(db.Transportadoras, "IdTransportadora", "NomeTransportadora", frete.IdTransportadora);
            return View(frete);
        }

        // GET: Fretes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Frete frete = db.Fretes.Find(id);
            if (frete == null)
            {
                return HttpNotFound();
            }
            return View(frete);
        }

        // POST: Fretes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Frete frete = db.Fretes.Find(id);
            db.Fretes.Remove(frete);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public JsonResult GetClientes(int idtransportadora)
        {
            var clientreject = new HashSet<int>(db.Fretes.Where(f=>f.IdTransportadora.Equals(idtransportadora)).Select(x => x.IdCliente));
            var cliente = db.Clientes.Where(x => !clientreject.Contains(x.IdCliente));
            return Json(cliente, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
