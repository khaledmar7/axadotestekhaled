Este � o projeto de avalia��o do processo seletivo da empresa Axado

Conforme escopo enviado no email segue abaixo as especifica��es:

O programa foi desenvolvido utilizando Visual Studio 2015 com .NET Framework 4.5.2

Como persistencia de dados foi utilizado o EntityFramework 6.3.1 e Fluent Api incluso no projeto

Para a aplica��o web utilizei ASP.NET 4.5.2 e ASP .NET MVC 5



Antes de executar a aplica��o � necess�rio apontar o seu servidor de banco de dados que a aplica��o
ir� criar a base de dados e suas tabelas

No projeto axadotestekhaled.Web abra o arquivo web.config e altere as informa��es da
connection string que cont�m colchetes

<connectionStrings>
    <add name="DataContext" 
         connectionString="Data Source=[seuservidor];       
         Initial Catalog=axadotestekhaled;       
         Integrated Security=True;       
         user id=[seu usuario de acesso];       
         password=[sua senha];       
         Pooling=False" 
         providerName="System.Data.SqlClient" />
  </connectionStrings>

